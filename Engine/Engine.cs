﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGameEngine.Engine {
    public class Engine : EngineBase {
        public void ShuffleZone(String zoneName) {
            if (!this.ZoneExists(zoneName)) return;

            int i_rand = 0;
            for (int i = 1; i < this.zones[zoneName].Count; i++) {
                i_rand = rnd.Next(0, i + 1);

                Card c1 = this.zones[zoneName][i];
                this.zones[zoneName][i] = this.zones[zoneName][i_rand];
                this.zones[zoneName][i_rand] = c1;
            }
        }

        public void Shuffle(String zoneName) {
            if (!this.ZoneExists(zoneName)) return;

            int i_rand = 0;
            for (int i = 1; i < this.CountZone(zoneName); i++) {
                i_rand = rnd.Next(0, i + 1);

                this.MoveCard(i, zoneName, i_rand, zoneName);
            }
        }
    }
}
