﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGameEngine.Engine {
    public class Zone : List<Card> {
        //Find and return Card by id else null
        public Card FindCard(long id) {
            foreach (Card c in this) {
                if (c.GetID() == id) {
                    return c;
                }
            }

            return null;
        }

    }
}
