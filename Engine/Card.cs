﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGameEngine.Engine {
    public class Card {
        private Dictionary<String, String> map;
        private long id;

        public static String ID_FIELD = "ID";

        public Card(long id) {
            this.map = new Dictionary<String, String>();
        }

        public long GetID() {
            return id;
        }

        public String Get(String key) {
            String ret = null;
            if (this.map.ContainsKey(key)) {
                ret = this.map[key];
            }
            return ret;
        }

        public void Set(String key, String value) {
            if (this.map.ContainsKey(key)) {
                this.map[key] = value;
            }
            else {
                this.Add(key, value);
            }
        }

        public void Add(String key, String val) {
            if (this.map.ContainsKey(key)) {
                this.map[key] = val;
            }
            else {
                this.map.Add(key, val);
            }
        }

        public void Copy(Card card) {
            foreach (String k in card.map.Keys) {
                this.Add(k, card.Get(k));
            }
        }
    }
}
