﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;

namespace CardGameEngine.Engine {
    public abstract class EngineBase {
        public Dictionary<String, Zone> zones;
        private long id;

        protected Random rnd;

        public EngineBase() {
            this.rnd = new Random();

            this.zones = new Dictionary<string, Zone>();
            this.id = 0;
        }

        public long GenerateId() {
            long id = this.id;
            if (this.GetCard(id) != null) {
                id = (DateTime.Now.Ticks / 10) % 1000000000;
            }
            
            return id++;
        }

        public bool ZoneExists(String name) {
            return zones.ContainsKey(name);
        }

        public long AddCard(Card c, String zone) {
            this.zones[zone].Add(c);
            return id;
        }

        public Card GetCard(long id) {
            Card ret = null;
            foreach (String key in this.zones.Keys) {
                foreach (Card c in this.zones[key]) {
                    if (c.GetID() == id) {
                        ret = c;
                    }
                }
            }
            return ret;
        }

        public String GetCardZone(long id) {
            String ret = "";
            foreach (String key in this.zones.Keys) {
                foreach (Card c in this.zones[key]) {
                    if (c.GetID() == id) {
                        ret = key;
                    }
                }
            }
            return ret;
        }

        public int CountZone(String zoneName) {
            return this.zones[zoneName].Count;
        }

        //Create a new zone
        public void CreateZone(String zoneName) {
            if (this.ZoneExists(zoneName)) return;

            this.zones.Add(zoneName, new Zone());
        }

        //Read cards
        public Card ReadZone(String zoneName, int position) {
            if (!this.ZoneExists(zoneName)) return null;
            if (position < 0) return null;
            if (position > this.zones[zoneName].Count) return null;

            return this.zones[zoneName][position];
        }

        //Insert cards
        public void UpdateZoneInsert(String zoneName, int position, Card c) {
            if (!this.ZoneExists(zoneName)) return;
            if (position < 0) return;
            if (position > this.zones[zoneName].Count) return;

            this.zones[zoneName].Insert(position, c);
        }

        //Delete cards
        public void UpdateZoneDelete(String zoneName, int position) {
            if (!this.ZoneExists(zoneName)) return;
            if (position < 0) return;
            if (position > this.zones[zoneName].Count) return;

            this.zones[zoneName].RemoveAt(position);
        }

        //Read operation for card
        public String ReadCard(Card card, String stat) {
            return card.Get(stat);
        }
        
        //Add or Insert new stats
        public void UpdateCard(Card card, String stat, String newVal) {
            card.Set(stat, newVal);
        }

        //Move cards to the target zone
        public void MoveCard(int fromPosition, String fromZone, int toPosition, String toZone) {
            if (!this.ZoneExists(fromZone)) return;
            if (!this.ZoneExists(toZone)) return;
            if (toPosition > this.CountZone(toZone)) return;
            if (toPosition < 0) return;
            if (fromPosition > this.CountZone(fromZone)) return;
            if (fromPosition < 0) return;

            Card c = this.ReadZone(fromZone, fromPosition);
            this.UpdateZoneDelete(fromZone, fromPosition);
            this.UpdateZoneInsert(toZone, toPosition, c);
        }
    }



}
